﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class Course
    {

        [Key]
        public int courseID { get; set; }
        [Required(ErrorMessage = "Se requiere ingresar el nombre de la asignatura")]
        public string subject { get; set; }
        public int maxStudents { get; set; }
        [Required(ErrorMessage = "Se requiere ingresar el docente de la cátedra")]
        public string teacher { get; set; }

        public virtual ICollection<Enrollment> enrollments { get; set; }


    }
}
