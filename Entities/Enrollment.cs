﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    public class Enrollment
    {
        [Key]
        public int enrollmentID { get; set; }

        public int recordID { get; set; }

        public int courseID { get; set; }

        public DateTime enrollmentDate { get; set; }
        public States state { get; set; }

        public virtual Course course { get; set; }
        public virtual Student student { get; set; }
    }


    public enum States
    {
        Inscripto = 1,
        Regular = 2,
        Libre = 3
    }
}
