﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class Student
    {

        [Key]
        public int recordID { get; set; }


        [Required(ErrorMessage = "Se requiere el nombre del alumno")]
        public string name { get; set; }

        [Required(ErrorMessage = "Se requiere la fecha de nacimiento")]
        public DateTime birthDate { get; set; }

        public int age
        {
            get
            {
                int _difference = DateTime.Today.Year - birthDate.Year;
                return _difference;
            }
        }

        //Usado para una simple validacion en el login
        public bool admin { get; set; }
        public string password { get; set; }

        //Lista de inscripciones para un alumno
        public virtual ICollection<Enrollment> Inscripciones { get; set; }


    }
}
