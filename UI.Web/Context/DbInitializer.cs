﻿using System;
using System.Collections.Generic;
using Entities;

namespace UI.Web.Context
{
    public class DbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<UniversityContext>
    {
        /*Usando el modelo de programacion code first:
        Cuando se cree o modifique el esquema de clases (entidades), la db va a ser dropeada y esta funcion se va a llamar para introducir datos una vez que se haya vuelto a crear.
        Para aplicar esta metologia se debe modificar el web.config dentro de las etiquetas 
        <entityFramework>
            <context>
                <databaseInitializer /> 
            </context>
        
            ...

        */
        protected override void Seed(UniversityContext context)
        {
            var Students = new List<Student>
            {
                new Student{name="Admin", birthDate =DateTime.Parse("07-01-2018"), admin=true, password="123"},
                new Student{name="Ana Alvarez", birthDate=DateTime.Parse("07-04-1993"), admin= false, password="123"},
                new Student{name="Franisco Fernandez", birthDate = DateTime.Parse("08-03-1994"), admin=false, password="123"},
                new Student{name="Pedro Piras", birthDate = DateTime.Parse("09-04-1992"), admin= false, password="123"}
            };

            Students.ForEach(a => context.Students.Add(a));
            context.SaveChanges();

            var Courses = new List<Course>
            {
                new Course{courseID=1,subject="Matematica", maxStudents=50, teacher="Bruno Bianco"},
                new Course{courseID=2, subject="Programacion", maxStudents=40, teacher="Carlos Calzada"},
                new Course{courseID=3,subject="Algebra", maxStudents=50, teacher="Maria Moriconi"}
            };

            Courses.ForEach(c => context.Courses.Add(c));
            context.SaveChanges();

            var Enrollmentes = new List<Enrollment>
            {
                new Enrollment{enrollmentID = 1, recordID=1, courseID=1, state=States.Inscripto, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 2, recordID=1, courseID=3, state=States.Regular, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 3, recordID=1, courseID=2, state=States.Regular, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 4, recordID=2, courseID=1, state=States.Inscripto, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 5, recordID=2, courseID=2, state= States.Regular, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 6, recordID=2, courseID=3, state=States.Libre, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 7, recordID=3, courseID=1, state=States.Libre, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 8, recordID=3, courseID=2, state=States.Regular, enrollmentDate=DateTime.Parse("15-03-2018")},
                new Enrollment{enrollmentID = 9, recordID=3, courseID=3, state=States.Regular, enrollmentDate=DateTime.Parse("15-03-2018")}
            };

            Enrollmentes.ForEach(i => context.Enrollments.Add(i));
            context.SaveChanges();

        }

    }
}