﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Entities;
namespace UI.Web.Context
{
    public class UniversityContext : DbContext
    {

        public UniversityContext() : base("UniversityContext")
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();
        }
    }
}