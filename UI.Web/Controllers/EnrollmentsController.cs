﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Entities;
using UI.Web.Context;

namespace UI.Web.Controllers
{
    public class EnrollmentsController : Controller
    {
        private UniversityContext db = new UniversityContext();


        //GET: Recursantes
        public ActionResult Recursantes()
        {
            try
            {
                var recursantes = from r in db.Enrollments
                                  .Where(r => r.state == States.Libre)
                                  select r;

                return View(recursantes.ToList());
            }
            catch(Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Enrollments", "Recursantes"));
            }

        }

        //GET: Inscripciones de un alumno en el ciclo lectivo actual
        public ActionResult Inscripciones(int? recordID)
        {
            try
            {
                if (recordID == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest); 
                }

                var inscripciones = from s in db.Enrollments
                                  .Where(s => s.recordID == recordID && s.state == States.Inscripto && 
                                  (s.enrollmentDate > DbFunctions.CreateDateTime(DateTime.Today.Year, 03, 01, 0, 0 ,0) && s.enrollmentDate < DbFunctions.CreateDateTime(DateTime.Today.Year +1, 02, 28, 0,0,0)))
                                  select s;           

                return View(inscripciones.ToList());
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Enrollments", "Inscripciones"));
            }

        }

        //Get: Estado académico
        public ActionResult EstadoAcademico(int? recordID)
        {
            try
            {
                if (recordID == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                //Materias cursadas
                var enrollments = from e in db.Enrollments
                                  .Where(e => e.recordID == recordID).ToList()
                                  select e;
                //todas las materias
                var CoursesAll = db.Courses.ToList();

                //Tenemos que unir las materias que curso con el total de materias poniendo las que no curso como libre
                //Iteramos sobre todas las materias y sobre las cursadas. SI coinciden la agregamos ccon su estado
                //si no la encontramos dentro de las cursadas las agregamos a la lista como libres
                List<Enrollment> totalMaterias = new List<Enrollment>();
                Enrollment en;
                bool found = false;
                foreach(Course c in CoursesAll)
                {                
                    foreach (Enrollment e in enrollments)
                    {             
                        if (e.courseID == c.courseID)
                        {
                            found = true;
                            en = new Enrollment();
                            en = e;
                            totalMaterias.Add(en);
                            break;
                        }                     
                    }

                    if (!found)
                    {
                        en = new Enrollment();
                        en.courseID = c.courseID;
                        en.state = States.Libre;
                        en.course = c;
                        totalMaterias.Add(en);
                    }
                        found = false;
             
                }

                return View(totalMaterias.ToList());
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Enrollments", "EstadoAcademico"));
            }
        }

        //GET: Inscripciones posibles
        //Todas las materias que no regularizo y las que aun no se inscribio
        public ActionResult InscripcionCursado(int? recordID)
        {
            try
            {

                //Materias cursadas
                var enrollments = from e in db.Enrollments
                                  .Where(e => e.recordID == recordID).ToList()
                                  select e;
                //todas las materias
                var CoursesAll = db.Courses.ToList();

                //Logica parecida a estado academico solo que tambien filtramos por aquellas materias que esta libre. Si esta libre la agregamos
                List<Enrollment> totalMaterias = new List<Enrollment>();
                Enrollment en;
                bool found = false;
                foreach (Course c in CoursesAll)
                {
                    foreach (Enrollment e in enrollments)
                    {
                        if (e.courseID == c.courseID)
                        {
                            if(e.state == States.Libre)
                            {
                                en = new Enrollment();
                                en = e;
                                totalMaterias.Add(en);
                            }
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        en = new Enrollment();
                        en.courseID = c.courseID;
                        en.state = States.Libre;
                        en.course = c;
                        totalMaterias.Add(en);
                    }
                    found = false;

                }

                return View(totalMaterias.ToList());
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Enrollments", "InscripcionCursado"));
            }
        }

        


        //Inscripcion POST
        [HttpPost]
        public ActionResult RealizarInscripcion(int? courseID)
        {
            int recordID_currentStudent = Convert.ToInt32(Session["recordID"]);

            if(courseID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Validar cupo
            var inscripcionesActuales = db.Enrollments.Where(e => e.courseID == courseID &&
                                        (e.enrollmentDate > DbFunctions.CreateDateTime(DateTime.Today.Year, 03, 01, 0, 0, 0) && e.enrollmentDate < DbFunctions.CreateDateTime(DateTime.Today.Year + 1, 02, 28, 0, 0, 0)));

            int cantidadInscripciones = inscripcionesActuales.Count();

            Course cursoDeseado = db.Courses.Find(courseID);

            if (cantidadInscripciones > cursoDeseado.maxStudents)
            {
                ViewBag.message = "No hay cupo disponible";
                return View();
            }
            
            Enrollment inscripcion = new Enrollment();
            inscripcion.courseID = Convert.ToInt32(courseID);
            inscripcion.recordID = recordID_currentStudent;
            inscripcion.enrollmentDate = DateTime.Today;
            inscripcion.state = States.Inscripto;

            //Si ya estaba inscripto a la catedra, actualizar su estado

            Enrollment inscripcionExistente = db.Enrollments.Where(e => e.courseID == courseID && e.recordID == recordID_currentStudent).FirstOrDefault();
            if (inscripcionExistente != null)
            {   
                inscripcionExistente.state = States.Inscripto;
                db.Entry(inscripcionExistente).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                db.Enrollments.Add(inscripcion);
                db.SaveChanges();
            }
            return Redirect("InscripcionCursado");
        }

        // GET: Enrollments/Create
        public ActionResult Create()
        {
            ViewBag.courseID = new SelectList(db.Courses, "courseID", "subject");
            ViewBag.recordID = new SelectList(db.Students, "recordID", "name");
            return View();
        }

        // POST: Enrollments/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "enrollmentID,recordID,courseID,enrollmentDate,state")] Enrollment enrollment)
        {
            if (ModelState.IsValid)
            {
                db.Enrollments.Add(enrollment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.courseID = new SelectList(db.Courses, "courseID", "subject", enrollment.courseID);
            ViewBag.recordID = new SelectList(db.Students, "recordID", "name", enrollment.recordID);
            return View(enrollment);
        }

        

 
    }
}
