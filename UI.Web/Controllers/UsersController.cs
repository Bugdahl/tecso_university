﻿using System.Net;
using System.Web.Mvc;
using Entities;
using System;

namespace UI.Web.Controllers
{
    public class UsersController : Controller
    {

        

        Context.UniversityContext db = new Context.UniversityContext();

        //Logout
        public ActionResult Logout()
        {
            Session.Abandon();
            return Redirect("/Home");

        }


        //Login
        public ActionResult Login()
        {
            if (Session["recordID"] != null)
            {
                return Redirect("/Home/Menu");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(int? recordID, string password)
        {
            try
            {
                string returnUrl = Url.Action("Index", "Home/Menu");
                if (recordID == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }


                Student student = db.Students.Find(recordID);
                if (student == null)
                {
                    ViewBag.Message = "Credenciales incorrectas"; return View();
                }

                if (student.password == password)
                {
                    Session["recordID"] = recordID;
                    Session["admin"] = student.admin;
                    Session["nombre"] = student.name;
                    return Redirect("../../Home/Menu");
                }
                else
                {

                    ViewBag.Message = "Credenciales incorrectas";
                    return View();
                }
            }
            catch(Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "User", "Login"));
            }
           

        }

    }
}